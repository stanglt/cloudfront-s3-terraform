variable project {
    type = string
    default = "playground"
}

variable environment {
    type = string
    default = "playground"
}

variable user {
    type = string
}