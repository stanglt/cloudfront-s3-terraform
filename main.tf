provider "aws" {
    region = "eu-central-1"
}

locals {
  bucket_name= "${var.project}-${var.environment}-client"
}

resource "aws_s3_bucket" "cf-bucket" {
    bucket = local.bucket_name
    acl = "private"
    
    tags = {
        Environment = var.environment,
        user = var.user
    }
}

data "aws_iam_policy_document" "cf-bucket-policy"  {
    statement {
       sid = "1"

        actions = [
            "s3:GetObject",
            "s3:ListBucket"
        ]
        
        resources = [
      "${aws_s3_bucket.cf-bucket.arn}/*",
      aws_s3_bucket.cf-bucket.arn
    ]

        principals {
            type = "AWS"

            identifiers = [
                aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
            ]
    }
    }
}

resource "aws_s3_bucket_policy" "cf-bucket-policy" {
    bucket = aws_s3_bucket.cf-bucket.id
    policy = data.aws_iam_policy_document.cf-bucket-policy.json
}

resource "aws_cloudfront_distribution" "main" {
  depends_on = [
    aws_s3_bucket.cf-bucket
  ]

    origin {
    domain_name = aws_s3_bucket.cf-bucket.bucket_regional_domain_name
    origin_id   = "s3-cloudfront"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    allowed_methods = [
      "GET",
      "HEAD",
    ]

    cached_methods = [
      "GET",
      "HEAD",
    ]

    target_origin_id = "s3-cloudfront"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  wait_for_deployment = false
    
    tags = {
        Environment = var.environment,
        user = var.user
    }
}

resource "aws_s3_bucket_object" "index" {
    bucket = aws_s3_bucket.cf-bucket.bucket
    key = "index.html"
    source = "${path.module}/index.html"
    content_type = "text/html"
    etag = filemd5("${path.module}/index.html")
}

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "access-identity-${local.bucket_name}.s3.amazonaws.com"
}